# CryptoPartyLDN posters

This repository contains posters and other graphics developed for
[CryptoPartyLDN](https://cryptoparty.in/london/) starting from
December 2017.

On a GNU/Linux system, PDF and PNG versions of the posters can be
generated using the [Inkscape](https://inkscape.org/) command-line
interface. For instance,

    $ inkscape file.svg --export-png=file.png --export-dpi=300

The original content of this repository is released into the public
domain (see `LICENCE.md`), copies and derivative work are welcome.

## Credits

Original design and editing work has been done by CryptoPartyLDN using
[Inkscape](https://inkscape.org/), [GIMP](https://www.gimp.org/),
[ImageMagick](http://www.imagemagick.org/), and other [Free
Software](https://www.fsf.org/) tools and applications.

Some of the images used in the posters have been taken from
[Unsplash](https://unsplash.com/).

Credits and links to original images, fonts, and other graphics
element have been added when due. If credit to the work of a third
party is missing, please contact the maintainers of this repository
and things will be amended as soon as possible.

## Contacts

CryptoPartyLDN is organised by [Big Brother
Watch](https://bigbrotherwatch.org.uk/), a UK non-profit campaigning
for privacy and civil liberties in the UK, and [Reckon
Digital](https://reckondigital.com/), a London-based software
company. We are supported by an amazing network of infosec trainers
and privacy experts.

For questions about this repository, please send us a direct message
on our Twitter or Mastodon accounts.

- https://twitter.com/cryptopartyldn
- https://mastodon.earth/@cryptopartyldn
